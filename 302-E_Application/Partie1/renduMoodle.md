# Remise projet Responsive (M Lafourcade)

* nommez votre fichier - archive zip , selon le schéma suivant :

	* HMIN302-RESPONSIVE-2020-<Vos noms de famille>

exemple: HMIN302-RESPONSIVE-2020-LafourcadeMachinToto

dans l'archive, il y  aura un fichier lisez-moi, qui m'indiquera ce que je dois faire pour installer faire fonctionner votre projet + URL de votre projet déployé.

Par ailleurs, envoyez moi un mail avec des proposition de date de soutenance (15 min en visio) à partir de maintenant (surtout ceux qui partent bientôt en stage).

cordialement,

mathieu lafourcade

Statut de remise
Statut des travaux remis 	Aucune tentative
Statut de l'évaluation 	Non évalué
Date de remise 	lundi 1 février 2021, 00:00